//
//  GameBoardCellModel.swift
//  HomeWork
//
//  Created by piro on 13.01.21.
//

import Foundation

enum GameBoardCellType {
    case empty
    case full
}

class GameBoardCellModel: NSObject {
    var number: Int
    var type: GameBoardCellType
    
    init(number: Int, type: GameBoardCellType) {
        self.number = number
        self.type = type
    }
}
