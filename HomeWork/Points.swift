//
//  Points.swift
//  HomeWork
//
//  Created by piro on 15.01.21.
//

import Foundation

class Points: NSObject {
    var arrayNames: [String] = []
    var arrayPoints: [String] = []
    
    var name: String
    var point: String
    
    
    func addArrayOfPoints() {
        arrayNames.append(name)
        arrayPoints.append(point)
    }
    
    init(name: String, point: String) {
        self.name = name
        self.point = point
    }
    
}

//var arrayNames: [String]
//var arrayPoints: [String]
//
//func addArrayOfPoints(name: String, point: String) {
//    arrayNames.append(name)
//    arrayPoints.append(point)
//}
//
//init(arrayNames: [String], arrayPoints: [String]) {
//    self.arrayNames = arrayNames
//    self.arrayPoints = arrayPoints
//}
