//
//  EasyViewController.swift
//  HomeWork
//
//  Created by piro on 17.12.20.
//

import UIKit
import Foundation

enum Level: Int {
    case lvlTwo = 2
    case easy
    case medium
    case hard
    case lvlSix
    case lvlSeven
}

struct Index {
    var row: Int
    var column: Int
}

class GameBoardViewController: UIViewController {
    
    @IBOutlet var timerL: UILabel!
    @IBOutlet var boardContainer: UIView!
    @IBOutlet var pauseAndResumeButton: UIButton!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var countOfPontsLabel: UILabel!
    
    var countOfPoints: Int = 0
    var x: CGFloat = 0
    var y: CGFloat = 0
    var newArray: [Int] = []
    var counts = 0
    var timer: Timer?
    var time = 0
    var pause = true
    var name: String = ""
    var newView: UIView!
    
    var currentGameLevel: Level = .medium
    var data: [[GameBoardCellModel]] = []
    var emptyCell: GameBoardCellModel?
    
    var spaceBetweenBlocks: CGFloat = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // создание ячеек
        for row in 0..<currentGameLevel.rawValue{
            var rowArray: [GameBoardCellModel] = []
            for column in 1...currentGameLevel.rawValue{
                let currentCellNumber = row * currentGameLevel.rawValue + column // для 3 строки: 2*3 + 1/2/3
                var type: GameBoardCellType
                if currentCellNumber != currentGameLevel.rawValue * currentGameLevel.rawValue {
                    type = .full
                } else {
                    type = .empty
                }
                let cell = GameBoardCellModel(number: currentCellNumber, type: type)
                rowArray.append(cell)
                if type == .empty {
                    emptyCell = cell // храним пустую ячейку
                }
            }
            data.append(rowArray)
            
        }
        
        setView()
        createTimer()
        updateBoard()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    // MARK: - Functions
    func setView() {
        countOfPontsLabel.text = String(0)
        nameLabel.text = name
        
        nameLabel.addDefaultSetting()
        countOfPontsLabel.addDefaultSetting()
        pauseAndResumeButton.addDefaultSetting()
        resetButton.addDefaultSetting()
        backButton.addDefaultSetting()
    }
    
    func createTimer() {
        timerL.font = UIFont(name: "Langar-Regular", size: 70)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(actionTimer), userInfo: nil, repeats: true)
    }
    
    @objc func actionTimer() {
        time += 1
        if time > 0 {
            let minutes = time / 60
            let seconds = time % 60
            timerL.text = "\(minutes):\(seconds)"
        }
    }
    
    // функция заполнения, которая  заполняет таблицу
    func updateBoard() { // перед тем как заполнять поле, удаляем ячейки
        for view in boardContainer.subviews {
            view.removeFromSuperview()
        }
        
        let size = (boardContainer.frame.width - spaceBetweenBlocks * CGFloat(currentGameLevel.rawValue) ) / CGFloat(currentGameLevel.rawValue)
        
        var x: CGFloat = 0
        var y: CGFloat = 0
        
        for row in data {
            for cellModel in row {
                if let cell = UINib(nibName: "GameBoardCell", bundle: nil).instantiate(withOwner: self, options: nil).first as? GameBoardCell {
                    cell.frame = CGRect(x: x, y: y, width: size, height: size)
                    cell.setupCell(model: cellModel)
                    cell.setSetting()
                        // экшн по нажатию на ячейку 
                    cell.onAction = { [unowned self] in
                                                self.moveEmptyCellToIndex(indexOfCell(cellModel))
                                                self.checkIfGameIsOver()
                    }
                    
                    boardContainer.addSubview(cell)
                }
                x += size + spaceBetweenBlocks
            }
            x = 0
            y += size + spaceBetweenBlocks
        }
    }
    // меняет ячеки местами и перерисовывает поля
    func swapTwoCells(_ firstCellModel: GameBoardCellModel, secondCellModel: GameBoardCellModel) {
        guard let firstIndex = indexOfCell(firstCellModel),
              let secondIndex = indexOfCell(secondCellModel) else {
            return
        }
        
        data[firstIndex.row][firstIndex.column] = secondCellModel
        data[secondIndex.row][secondIndex.column] = firstCellModel
        updateBoard()
        
        countOfPoints += 1
        countOfPontsLabel.text! = String(countOfPoints)
    }
    
    // проверка на решение игры
    func checkIfGameIsOver() {
        var numberOfCorrectElements = 0 // число элементов которые находятс на своих местах
        
        for row in 0..<currentGameLevel.rawValue {
            for column in 0..<currentGameLevel.rawValue {
                let currentCellNumber = row * currentGameLevel.rawValue + column + 1
                if currentCellNumber == data[row][column].number {
                    numberOfCorrectElements += 1
                }
            }
        }
        
        if numberOfCorrectElements == currentGameLevel.rawValue * currentGameLevel.rawValue {
            print("game over")
            timer?.invalidate()
            pause = false
            
            let alert = UIAlertController(title: "🥳🎉", message: "counts: \(String(countOfPoints))", preferredStyle: .alert)
            let alertOk = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(alertOk)
            present(alert, animated: true, completion: nil)
    
        }
    }
    
    // двигает пустую ячейку на индекс
    func moveEmptyCellToIndex(_ index: Index?) {
        guard let emptyCell = emptyCell,
              let index = index,
              var emptyCellIndex = indexOfEmptyCell() else {
            return
        }
        
        if emptyCellIndex.row == index.row { // проверяет находиться ли пустая ячейка на той же строке
            let delta = emptyCellIndex.column > index.column ? -1 : 1 // вычисляем куда нужно двигаться. если индекст у пустой больше чем на индекс нажатой
            while emptyCellIndex.column != index.column  { // цикл пока пустой стобец не станет равен куда мы хотим переместится
                let indexToSwap = Index(row: emptyCellIndex.row, column: emptyCellIndex.column + delta)
                swapTwoCells(emptyCell, secondCellModel: data[indexToSwap.row][indexToSwap.column])
                emptyCellIndex = indexToSwap
            }
        } else if emptyCellIndex.column == index.column {
            let delta = emptyCellIndex.row > index.row ? -1 : 1
            while emptyCellIndex.row != index.row  {
                let indexToSwap = Index(row: emptyCellIndex.row + delta, column: emptyCellIndex.column)
                swapTwoCells(emptyCell, secondCellModel: data[indexToSwap.row][indexToSwap.column])
                emptyCellIndex = indexToSwap
            }
        }

       
    }
    
    
    
    // ищёт индекс конкретной ячеки которую мы передадим
    func indexOfCell(_ cellModel: GameBoardCellModel?) -> Index? {
        guard let cellModel = cellModel else {
            return nil
        }
        for row in 0..<currentGameLevel.rawValue {
            for column in 0..<currentGameLevel.rawValue {
                let cell = data[row][column]
                if cell == cellModel {
                    return Index(row: row, column: column)
                }
            }
        }
        return nil
    }
    // находит индекс пустого элемента в массиве
    func indexOfEmptyCell() -> Index? {
        return indexOfCell(emptyCell)
    }
    


// MARK: - Actions
@IBAction func BackOnPressed(_ sender: UIButton) {
    dismiss(animated: true, completion: nil)
}

@IBAction func pauseAndResumeOnButton(_ sender: UIButton) {
    if pause {
        timer?.invalidate()
        pause = false
        pauseAndResumeButton.setTitle("Resume", for: .normal)
    } else {
        createTimer()
        pause = true
        pauseAndResumeButton.setTitle("Pause", for: .normal)
    }
}

@IBAction func reset(_ sender: UIButton) {
    timer?.invalidate()
    time = 0
    
    countOfPoints = 0
    countOfPontsLabel.text! = String(countOfPoints)
    
    data = []
    
    for view in boardContainer.subviews {
        view.removeFromSuperview()
    }
    
    createTimer()
    for row in 0..<currentGameLevel.rawValue{
        var rowArray: [GameBoardCellModel] = []
        for column in 1...currentGameLevel.rawValue{
            let currentCellNumber = row * currentGameLevel.rawValue + column // для 3 строки: 2*3 + 1/2/3
            var type: GameBoardCellType
            if currentCellNumber != currentGameLevel.rawValue * currentGameLevel.rawValue {
                type = .full
            } else {
                type = .empty
            }
            let cell = GameBoardCellModel(number: currentCellNumber, type: type)
            rowArray.append(cell)
            if type == .empty {
                emptyCell = cell // храним пустую ячейку
            }
        }
        data.append(rowArray)
        
        
    }
    updateBoard()
}

}

