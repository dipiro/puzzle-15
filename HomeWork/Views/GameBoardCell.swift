//
//  GameBoardCell.swift
//  HomeWork
//
//  Created by piro on 13.01.21.
//

import Foundation
import UIKit

class GameBoardCell: UIView {

    @IBOutlet private weak var numberLabel: UILabel!
    
    // для добавления экшна на нажатия по кнопке
    var onAction: (()->())?
    
    @IBAction private func onButtonPress(_ sender: Any) {
        if let action = onAction {
            action()
        }
    }
    
    func setupCell(model: GameBoardCellModel) {
        numberLabel.text = String(model.number)
        if model.type == .empty {
            isUserInteractionEnabled = false
            alpha = 0
        }
    }
    
    func setSetting() {
        self.layer.cornerRadius = 16
        
        self.backgroundColor = .black
        
        numberLabel.font = UIFont(name: "Langar-Regular", size: 24)
        numberLabel.textColor = .white
    
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 1
    }
}
