//
//  ViewController.swift
//  HomeWork
//
//  Created by piro on 16.12.20.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var welcomeL: UILabel!
    @IBOutlet var containerAlertView: UIView!
    @IBOutlet var containerAlertViewBottomOutlet: NSLayoutConstraint!
    @IBOutlet var backgroundAlertView: UIView!
    @IBOutlet var buttonsB: [UIButton]!
    @IBOutlet var chooseModeSlider: UISlider!
    @IBOutlet var chooseModeLabel: UILabel!
    @IBOutlet var enterNameButton: UIButton!
    @IBOutlet var enterInAlertButton: UIButton!
    
    var backgroundAlertView1: UIView = UIView()
    var name: String = ""
    var date: String = ""
    var customGame = 0
    var dateTextField: UITextField?
    
    let dataPicker = UIDatePicker()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataPicker.preferredDatePickerStyle = .wheels
        dataPicker.addTarget(self, action: #selector(handlerPicker(_:)), for: .valueChanged)
        
        let mutableAttrStr = NSMutableAttributedString(string: "Welcome to the game Puzzle 15")
        let mutableParagStyle = NSMutableParagraphStyle()
        
        mutableAttrStr.addAttribute(.font, value: UIFont(name: "Langar-Regular", size: 24)!, range: NSRange(location: 0, length: mutableAttrStr.length))
        mutableAttrStr.addAttribute(.font, value: UIFont(name: "Langar-Regular", size: 30)!, range: NSRange(location: 20, length: 9))
        mutableAttrStr.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: 20, length: 9))
        mutableParagStyle.lineSpacing = 10
        mutableAttrStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: mutableParagStyle, range: NSRange(location: 0, length: mutableAttrStr.length))
        
        chooseModeSlider.value = 1
        chooseModeSlider.minimumValue = 2
        chooseModeSlider.maximumValue = 7
        chooseModeSlider.tintColor = .black
        chooseModeSlider.thumbTintColor = .black
        
        chooseModeLabel.addDefaultSetting()
        
        welcomeL.numberOfLines = 0
        welcomeL.attributedText = mutableAttrStr
        welcomeL.textAlignment = .center
        
        containerAlertView.backgroundColor = .white
        containerAlertView.layer.cornerRadius = 16
        containerAlertView.clipsToBounds = true
        
        containerAlertViewBottomOutlet.constant = (view.frame.maxY - containerAlertView.frame.minY + 100)
        
        let blurEffect = UIBlurEffect(style: .dark)
        let visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.frame = backgroundAlertView.bounds
        backgroundAlertView.addSubview(visualEffectView)
        
        enterInAlertButton.addDefaultSetting()

        enterNameButton.addDefaultSetting()
        
        for button in buttonsB {
            button.addDefaultSetting()
        }
        
        let tapHidenBackgoundAlert = UITapGestureRecognizer(target: self, action: #selector(tapHidenBackgoundAllertHandler(_:)))
        backgroundAlertView.addGestureRecognizer(tapHidenBackgoundAlert)
    }
    
    //MARK: -HANDLER DATAPICKER
    @objc func handlerPicker(_ sender: UIDatePicker) {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd-MMMM-yyyy"
        dateTextField?.text = dateFormator.string(from: sender.date)
        
    }
    
    @objc func tapHidenBackgoundAllertHandler(_ gesture: UITapGestureRecognizer) {
        if self.backgroundAlertView.alpha == 0 {
            containerAlertViewBottomOutlet.constant = 0
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.backgroundAlertView.alpha = 1
            }
        } else {
            containerAlertViewBottomOutlet.constant = (view.frame.maxY - containerAlertView.frame.minY + 100)
            self.backgroundAlertView.alpha = 0
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.backgroundAlertView.alpha = 0
            }
        }
        
    }
        
    func push() {
        let storyboard =  UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewContr = storyboard.instantiateViewController(withIdentifier: "easy") as! GameBoardViewController
        if let level = Level(rawValue: customGame) {
            viewContr.currentGameLevel = level
        }
        viewContr.modalPresentationStyle = .fullScreen
        present(viewContr, animated: true, completion: nil)
    }
    
    

    // MARK: - ACTION
    @IBAction func alertCustomDoneButton(_ sender: UIButton) {
        push()
    }
    
    @IBAction func alertCusom(_ sender: UIButton) {
        if self.backgroundAlertView.alpha == 0 {
            containerAlertViewBottomOutlet.constant = 0
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.backgroundAlertView.alpha = 1
            }
        } else {
            containerAlertViewBottomOutlet.constant = (view.frame.maxY - containerAlertView.frame.minY + 100)
            self.backgroundAlertView.alpha = 0
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.backgroundAlertView.alpha = 0
            }
        }
    }
    
    @IBAction func nextToPlayButtons(_ sender: UIButton) {
        let storyboard =  UIStoryboard(name: "Main", bundle: Bundle.main)
        let extractedExpr = Level(rawValue: sender.tag)
        if let controller = storyboard.instantiateViewController(withIdentifier: "easy") as? GameBoardViewController,
           let level = extractedExpr {
            controller.currentGameLevel = level
            controller.modalPresentationStyle = .fullScreen
            present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func actionSlider(_ sender: UISlider) {
        chooseModeLabel.text = "\(String(Int(sender.value))) x \(String(Int(sender.value)))"
        customGame = Int(sender.value)
    }
    
    @IBAction func enterYourName(_ sender: UIButton) {
        let alert = UIAlertController(title: "Enter Your Info", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addTextField { (name) in
            name.placeholder = "Enter Your Name"
        }
        alert.addTextField()
        alert.textFields?[1].placeholder = "Enter Your Date"
        alert.textFields?[1].inputView = dataPicker
        dateTextField = alert.textFields?[1]
        let saveAction = UIAlertAction(title: "Ok", style: .default) { [self] (_) in
            if let nameTextField = alert.textFields?[0].text,
               let dateTextField = alert.textFields?[1].text {
                name = nameTextField
                date = dateTextField
                self.enterNameButton.setTitle(nameTextField, for: .normal)
            }
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
}






