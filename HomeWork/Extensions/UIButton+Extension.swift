//
//  UIButton + Extensions.swift
//  HomeWork
//
//  Created by piro on 26.12.20.
//

import Foundation
import UIKit

extension UIButton {
    
    func addDefaultSetting() {
        self.layer.cornerRadius = 16
        
        self.backgroundColor = .black
        
        self.titleLabel?.font = UIFont(name: "Langar-Regular", size: 24)
        self.setTitleColor(UIColor.white, for: .normal)
    
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 1
    }
    
    func addGradient(firstColor: UIColor, secondColor: UIColor) {
        let gradient = CAGradientLayer()
        
        gradient.colors = [UIColor.blue.cgColor, UIColor.green.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = bounds
        
        layer.insertSublayer(gradient, at: 0)
        
    }
}
