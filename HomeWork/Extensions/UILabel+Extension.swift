//
//  UILabel+Extension.swift
//  HomeWork
//
//  Created by piro on 4.01.21.
//

import Foundation
import UIKit

extension UILabel {
    
    func addDefaultSetting() {
        
        self.font = UIFont(name: "Langar-Regular", size: 30)
        
    }
    
}


